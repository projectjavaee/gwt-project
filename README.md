_GWT Project_
===================

_***Технологии проекта:***_

1. _Spring;_
2. _GWT;_
3. _Maven;_
4. _Hibernate;_
5. _PostgreSQL;_
6. _Jetty;_
7. _Tomcat;_
8. _и другие._

_***Для успешного скачивания и запуска проекта должно быть установлено следующее ПО:***_

1. _[JAVA 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html "Страница загрузки JAVA");_
2. _[Apache Maven](https://maven.apache.org/ "Сайт Maven");_
3. _[Git](https://git-scm.com/download "Страница загрузки Git");_
4. _[PostgreSQL](https://www.postgresql.org/download/ "Страница загрузки PostgreSQL")._

##_Сборка проекта_
_***Для восстановления зависимостей необходимо с помощью командной строки или терминала перейти в корневую папку проекта, где расположен файл [pom.xml](pom.xml) и выполнить команду ``mvn clean package``.***_


##_Создание базы данных_
_***Для создания базы данных небходимо в SQL Shell ввести команду ```create database card-file;```, либо воспользоваться [pgAdmin](https://www.pgadmin.org/ "Страница pgAdmin"), либо командной строкой или терминалом. 
После в файле [data-source.xml](gwt-project-server/src/main/resources/data-source.xml) в полях ```<property name="username" value="<?>"/>``` и ```<property name="password" value="<?>"/>``` вместо ```<?>``` указать учетные данные базы.***_


##_Запуск проекта_
_***Запуск проекта осуществляется из корневой папки проекта последовательным выполнением команд:***_

1. _```mvn gwt:codeserver -pl *-client -am```_
2. _```mvn jetty:run -pl *-server -am -Denv=dev```_
3. _Откройте в браузере ссылку [http://localhost:8080/](http://localhost:8080/)._



_``убить процесс``_ 
1. _```Ctrl + c```_
2. _```netstat -ano | findstr :9876```_ 
3. _```taskkill /PID <pid> /F```_
