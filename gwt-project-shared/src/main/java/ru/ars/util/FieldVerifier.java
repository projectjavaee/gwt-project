package ru.ars.util;

import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 20:54
 *
 * Util class
 */
public class FieldVerifier {

	public static boolean isValidName(String name) {
		return name != null && name.length() >= 1;
	}

	public static boolean isValidDate(Date date) {
		return date != null;
	}
}
