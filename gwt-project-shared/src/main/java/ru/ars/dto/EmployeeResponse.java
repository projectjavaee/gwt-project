package ru.ars.dto;

import ru.ars.domain.Employee;

import java.io.Serializable;
import java.util.Date;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 18:17
 *
 * DTO
 */
public class EmployeeResponse implements Serializable {

    private Long id;

    private String fullName;

    private Date birthdate;

    public EmployeeResponse() {
    }

    public EmployeeResponse(Long id, String fullName, Date birthdate) {
        this.id = id;
        this.fullName = fullName;
        this.birthdate = birthdate;
    }

    public EmployeeResponse(Employee emp) {
        this.id = emp.getId();
        this.fullName = emp.getFullName();
        this.birthdate = emp.getBirthdate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "EmployeeResponse(" +
                "id: " + id +
                ", fullName: " + fullName +
                ", birthdate: " + birthdate +
                ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmployeeResponse response = (EmployeeResponse) o;

        return id != null ? id.equals(response.id) : response.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
