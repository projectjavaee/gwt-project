package ru.ars.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.ars.dto.EmployeeResponse;

import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 18:14
 */
@RemoteServiceRelativePath("employee")
public interface EmployeeService extends RemoteService {

    List<EmployeeResponse> findAll();

    EmployeeResponse save(EmployeeResponse dto);

    void delete(Long id);
}
