package ru.ars.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.ars.domain.Employee;
import ru.ars.dto.EmployeeResponse;

import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 18:21
 */
public interface EmployeeServiceAsync {

    void findAll(AsyncCallback<List<EmployeeResponse>> callback);

    void save(EmployeeResponse dto, AsyncCallback<EmployeeResponse> callback);

    void delete(Long id, AsyncCallback<Void> callback);
}
