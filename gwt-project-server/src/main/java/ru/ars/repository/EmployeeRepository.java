package ru.ars.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ars.domain.Employee;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 19:34
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
