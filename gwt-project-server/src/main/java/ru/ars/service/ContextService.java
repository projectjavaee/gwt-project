package ru.ars.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 20:37
 *
 * Create ApplicationContext
 */
@Service
public class ContextService implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    private ContextService() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        ContextService.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}