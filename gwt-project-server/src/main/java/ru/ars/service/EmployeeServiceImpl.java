package ru.ars.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.ars.domain.Employee;
import ru.ars.dto.EmployeeResponse;
import ru.ars.repository.EmployeeRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 19:12
 *
 * Service class
 */
@SuppressWarnings("serial")
public class EmployeeServiceImpl extends RemoteServiceServlet implements EmployeeService {

    private EmployeeRepository repository = ContextService.getApplicationContext().getBean(EmployeeRepository.class);

    /**
     * Get all Employee
     * @return List<EmployeeResponse>
     */
    @Override
    public List<EmployeeResponse> findAll() {
        return repository
                .findAll()
                .stream()
                .map(EmployeeResponse::new)
                .collect(Collectors.toList());
    }

    /**
     * Create and update Employee
     * @param dto EmployeeResponse
     * @return EmployeeResponse
     */
    @Override
    public EmployeeResponse save(EmployeeResponse dto) {
        Employee employee = dto.getId() == null ? new Employee() : repository.getOne(dto.getId());
        employee.setFullName(dto.getFullName());
        employee.setBirthdate(dto.getBirthdate());
        employee = repository.saveAndFlush(employee);
        return new EmployeeResponse(employee);
    }

    /**
     * Delete Employee
     * @param id id Employee
     */
    @Override
    public void delete(Long id) {
        repository.findById(id).ifPresent(repository::delete);
    }
}
