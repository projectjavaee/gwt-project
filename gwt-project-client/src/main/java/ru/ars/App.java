package ru.ars;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.ListDataProvider;
import ru.ars.dto.EmployeeResponse;
import ru.ars.service.EmployeeService;
import ru.ars.service.EmployeeServiceAsync;
import ru.ars.util.FieldVerifier;

import java.util.Date;
import java.util.List;

/**
 * Developer: Ruslan Sabirov
 * Date: 16.02.2018 18:10
 *
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class App implements EntryPoint {

    /**
     * Create a remote service proxy to talk to the server-side Employee service.
     */
    private final EmployeeServiceAsync employeeService = GWT.create(EmployeeService.class);

    /**
     * The pattern format date
     */
    private final DateTimeFormat dtf = DateTimeFormat.getFormat("dd.MM.yyyy");

    /**
     * Indicates whether to add or edit
     */
    private boolean flag = true;

    /**
     * id Employee
     */
    private Long id = null;

    private final Label errorLabel = new Label();

    private final DialogBox dialogBox = new DialogBox();

    private final Button closeButton = new Button("Отмена");

    private final TextBox fullName = new TextBox();

    private final DateBox birthdate = new DateBox();

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        /*
         * Create table
         */
        CellTable<EmployeeResponse> table = new CellTable<>();

        TextColumn<EmployeeResponse> nameColumn = new TextColumn<EmployeeResponse>() {
            @Override
            public String getValue(EmployeeResponse employeeResponse) {
                return employeeResponse.getFullName();
            }
        };

        TextColumn<EmployeeResponse> dateColumn = new TextColumn<EmployeeResponse>() {
            @Override
            public String getValue(EmployeeResponse employeeResponse) {
                return dtf.format(employeeResponse.getBirthdate());
            }
        };

        final ButtonCell be = new ButtonCell();

        Column<EmployeeResponse, String> buttonEdit = new Column<EmployeeResponse, String>(be) {
            @Override
            public String getValue(EmployeeResponse o) {
                return "Изменить";
            }
        };

        final ButtonCell bd = new ButtonCell();

        Column<EmployeeResponse, String> buttonDelete = new Column<EmployeeResponse, String>(bd) {
            @Override
            public String getValue(EmployeeResponse object) {
                return "Удалить";
            }
        };

        table.addColumn(nameColumn, "ФИО");
        table.addColumn(dateColumn, "Дата рождения");
        table.addColumn(buttonEdit, "");
        table.addColumn(buttonDelete, "");

        /*
         * Create the button to add employees
         */
        final Button createButton = new Button("Добавить сотрудника");
        final Button saveButton = new Button("Сохранить");

        /*
         * Fill table
         */
        ListDataProvider<EmployeeResponse> dataProvider = new ListDataProvider<>();
        dataProvider.addDataDisplay(table);
        List<EmployeeResponse> employeeResponseList = dataProvider.getList();
        fillList(employeeResponseList);

        /*
         * Sort table
         */
        table.getColumnSortList().push(nameColumn);

        /*
         * Add a handler to send the name to the server
         */
        MyHandler handler = new MyHandler();

        buttonEdit.setFieldUpdater((i, employeeResponse, s) -> {
            flag = false;
            handler.createEmployee();
            id = employeeResponse.getId();
            fullName.setValue(employeeResponse.getFullName());
            birthdate.setValue(employeeResponse.getBirthdate());
        });

        buttonDelete.setFieldUpdater((i, employeeResponse, s) -> employeeService
                .delete(employeeResponse.getId(), new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable throwable) {
                        GWT.log("Throwable", throwable);
                    }

                    @Override
                    public void onSuccess(Void aVoid) {
                        employeeResponseList.remove(i);
                    }
                }));

        final VerticalPanel dialogVPanel = new VerticalPanel();

        /*
         * Add style names to widgets
         */
        errorLabel.addStyleName("errorLabel");
        dialogBox.addStyleName("dialogBox");
        createButton.addStyleName("createButton");
        dialogVPanel.addStyleName("dialogVPanel");

        /*
         * Use RootPanel.get() to get the entire body element
         */
        RootPanel.get("employeeContainer").add(table);
        RootPanel.get("createButtonContainer").add(createButton);
        RootPanel.get("errorLabelContainer").add(errorLabel);

        dialogBox.setAnimationEnabled(true);
        dialogBox.setGlassEnabled(true);

        /*
         * Set the id of a widget by accessing its Element
         */
        closeButton.getElement().setId("closeButton");
        saveButton.getElement().setId("saveButton");

        /*
         * Format date
         */
        birthdate.setFormat(new DateBox.DefaultFormat(dtf));
        fullName.setFocus(true);
        fullName.selectAll();

        /*
         * Create the elements for modal window
         */
        final HorizontalPanel fnPanel = new HorizontalPanel();
        Label label = new Label("ФИО");
        label.setWidth("70px");
        fnPanel.add(label);
        fnPanel.add(fullName);
        dialogVPanel.add(fnPanel);

        final HorizontalPanel bdPanel = new HorizontalPanel();
        label = new Label("Дата рождения");
        label.setWidth("70px");
        bdPanel.add(label);
        bdPanel.add(birthdate);
        dialogVPanel.add(bdPanel);

        dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);

        final HorizontalPanel butPanel = new HorizontalPanel();
        butPanel.add(closeButton);
        butPanel.add(saveButton);

        dialogVPanel.add(butPanel);
        dialogVPanel.add(errorLabel);

        dialogBox.setWidget(dialogVPanel);

        /*
         * Add a handler to close the DialogBox
         */
        closeButton.addClickHandler(event -> {
            flag = true;
            dialogBox.hide();
            fullName.setValue(null);
            birthdate.setValue(null);
            createButton.setFocus(true);
            createButton.setEnabled(true);
        });

        /*
         * Add a handler for the DialogBox when saving
         */
        saveButton.addClickHandler(clickEvent -> {

            String name = fullName.getValue();
            Date date = birthdate.getValue();

            if (!FieldVerifier.isValidName(name)) {
                errorLabel.setText("Пожалуйста, введите ФИО");
                return;
            }
            if (!FieldVerifier.isValidDate(date)) {
                errorLabel.setText("Пожалуйста, укажите дату");
                return;
            }

            employeeService.save(new EmployeeResponse(id, name, date), new AsyncCallback<EmployeeResponse>() {
                @Override
                public void onFailure(Throwable throwable) {
                    GWT.log("Throwable", throwable);
                }

                @Override
                public void onSuccess(EmployeeResponse employeeResponse) {

                    int index = employeeResponseList.indexOf(employeeResponse);

                    if (index > -1) {
                        employeeResponseList.set(index, employeeResponse);
                    } else {
                        employeeResponseList.add(employeeResponse);
                    }
                }
            });

            id = null;
            flag = true;
            dialogBox.hide();
            fullName.setValue(null);
            birthdate.setValue(null);
            createButton.setFocus(true);
            createButton.setEnabled(true);
        });

        createButton.addClickHandler(handler);
    }

    /**
     * Fill list for table
     *
     * @param employeeResponseList List<EmployeeResponse>
     */
    private void fillList(List<EmployeeResponse> employeeResponseList) {
        employeeService.findAll(new AsyncCallback<List<EmployeeResponse>>() {
            @Override
            public void onFailure(Throwable throwable) {
                GWT.log("Throwable", throwable);
            }

            @Override
            public void onSuccess(List<EmployeeResponse> employeeResponses) {
                employeeResponseList.addAll(employeeResponses);
            }
        });
    }

    /**
     * Create a handler for the createButton and buttonEdit
     */
    private class MyHandler implements ClickHandler, KeyUpHandler {

        private final HTML serverResponseLabel = new HTML();

        public void onClick(ClickEvent event) {
            createEmployee();
        }

        public void onKeyUp(KeyUpEvent event) {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                createEmployee();
            }
        }

        private void createEmployee() {
            String title = flag ? "Добавить нового сотрудника" : "Изменить данные сотрудника";
            errorLabel.setText("");
            dialogBox.setText(title);
            serverResponseLabel.removeStyleName("errorLabelContainer");
            dialogBox.center();
            closeButton.setFocus(true);
        }
    }
}
